#! /bin/sh

if test $# -ne 2; then
    echo "Usage:`basename $0 .sh` <process-id>cmd" 1>&2
    echo "For example:`basename $0 .sh` 1000 bt" 1>&2

fi
if test ! -r /proc/$1;then
    echo "process $1 not found." 1>&2
    exit 1
fi
result = ""
GDB=${GDB:-/usr/bin/gdb}
result=`$GDB --quiet -nx /proc/$1/exe $1 <<EOF 2>&1
&2
EOF`
echo "$result" |egrep -A 1000 "^\(gdb\)" |egrep -B 1000 -e "^\(gdb\)"

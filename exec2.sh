#!/bin/bash

exec 4<&0
exec <$1

exec 5>&1
exec >$2

cat - |tr a-z A-Z

exec 0<&4 4<&-
exec 1>&5 5>&-

echo "file \"$1\" written to \"$2\""
exit 0

#!/bin/bash

clean(){
    for pid in `ps aux | grep 'tail -f msg' | grep -v 'grep' | awk '{print $2}'`
    do 
        kill -9 $pid >/dev/null 2>&1 
    done 
    for pid in `ps aux | grep 'bin/bash ./receive.sh' | grep -v 'grep' | awk '{print $2}'` 
        do 
            kill -9 $pid >/dev/null 2>&1
        done
} 
stop(){
    clean
    exit 0;
} 
clean >msg
tail -f msg | ./receive.sh &
while read line 
    do
        if [ "$line" = "exit" ]; then 
                stop
        fi
        ./send.sh "$line" 10.20.151.97
    done
